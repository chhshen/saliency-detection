



%% add  libraries

clear all; close all;

addpath(genpath('./GraphSegment'));
addpath(genpath('./feature_util'));
addpath(genpath('./ClusteringUtil'));
addpath(genpath('./libsvm'));
addpath(genpath('./SaliencyDetectionLib'))




cpwd=pwd;
cd 'vlfeat/toolbox'
vl_setup

cd(cpwd)

%% Image and superpixel segmentation paths
ImagePath = './Images/';
ImgDir = dir(fullfile(ImagePath, '*.jpg'));

SuperpixelSegmentPath = './MeanShiftSegDir/'; % or  './SlicSegDir/'
SuperpixelSegDir = dir(fullfile(SuperpixelSegmentPath, '*.mat'));

%% Main function
for tt= 1:length(ImgDir)
    %% Load Image onto memory
    ImageName = strcat('./Images/', ImgDir(tt).name);
    im = double(imread(ImageName)); [m, n, c] = size(im);
    figure;
    
    imshow(uint8(im));


    %% Extract the superpixel information
    str = SuperpixelSegDir(tt).name;
    OverSegmentFilePath = strcat(SuperpixelSegmentPath, str);
    load(OverSegmentFilePath);
    imsegs.segimage = segments;
    spstats = regionprops(imsegs.segimage , 'PixelIdxList');
    num_region = max(imsegs.segimage(:));

    %% Extract the features for each superpixel
    [ColorArr, FeatArr] = ExtractColorFeatures(im, spstats);

    %% Obtain the gradient information
    Img_grad = GetImageGradientInfo(im);

    %% Hypergraph saliency by mean shift
    FinalContourMap_MS = MeanShiftClusteringHypergraphSaliency(Img_grad, FeatArr, spstats, num_region, m, n);

    %% SVM saliency
    option = 'WeightedLSSVM'; % or WeightedStandardSVM
    FinalContourMap_SVM = LocalSvmSaliency(im, m, n, option);

    %% Saliency fusion
    FusionOptionArr = {'MS+Graph+Hypergraph', 'MS+Hypergraph', 'NULL'};
    FusionOption = FusionOptionArr{1}; % This option is recommended for experiments with better results in general.
    switch FusionOption
        case 'MS+Graph+Hypergraph'
              %% Hypergraph saliency by graph clustering
               FinalContourMap_GC = GraphClusteringHypergraphSaliency(im, Img_grad, m, n);
               FinalFusedMap = log(1 + (0.6*FinalContourMap_MS + 0.3*FinalContourMap_GC + 0.1*FinalContourMap_SVM));  % logarithmic or exponentinal mapping after fusion
        case 'MS+Hypergraph'
               FinalFusedMap = log(1 + (0.9*FinalContourMap_MS + 0.1*FinalContourMap_SVM)); % logarithmic or exponentinal mapping after fusion
        otherwise
               disp('Please choose a correct option!');
               break;
    end
    % Linear normalization
    FinalFusedMap = (FinalFusedMap - min(FinalFusedMap(:))) / (max(FinalFusedMap(:)) - min(FinalFusedMap(:)));
    
    figure;
    imagesc(FinalFusedMap); colormap('gray');
    title('Fused saliency map');

    %% Graph smoothing for saliency postprocessing
    %superpixel graph smoothing
    ColorAffMat = exp(-dist(FeatArr(5:8,:))/0.05);
    FinalMap_Sp = zeros(num_region, 1);
    for ii=1:num_region
        FinalMap_Sp(ii) = mean(FinalFusedMap(spstats(ii).PixelIdxList));
    end
    D = diag(sum(ColorAffMat, 2).^(-0.5));
    RankingMat = inv(eye(num_region) - D*ColorAffMat*D*(1/2)); %graph ranking
    FinalScore_Sp = RankingMat*FinalMap_Sp;

    % pixel saliency computation
    RankedSalMap = zeros(m,n);
    for ii=1:num_region
        RankedSalMap(spstats(ii).PixelIdxList) =  FinalScore_Sp(ii);  %superpixel saliency assigned to its pixels
    end

    %% Final saliency map
    RankedSalMap = (RankedSalMap - min(RankedSalMap(:))) / (max(RankedSalMap(:)) - min(RankedSalMap(:))); % linear normalization
      
    figure;imagesc(RankedSalMap); colormap('gray');
    title('Final saliency map');
    
    disp 'press a key to continue ...'
    pause
end


