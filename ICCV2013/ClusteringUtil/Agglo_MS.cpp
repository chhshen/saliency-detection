// *************************************************************************
//  Agglomerative Mean-Shift Clustering
//
//  Reference: Xiao-Tong Yuan, Bao-Gang Hu and Ran He, Agglomerative Mean-Shift Clustering, IEEE Transactions on Knowledge and Data Engineering
//
//   Written by Xiao-Tong Yuan, 2009-2010.
// *************************************************************************

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA



#include "Agglo_MS.h"
#include <math.h>
#include <limits.h>
#include "mex.h"
#include <limits.h>
#include <memory.h>


Agglo_MS::Agglo_MS(double* Data, int dim, int N, double* ref_data, int ref_data_n, double* ref_weight,double band, int iten)
{
	data_dim = dim;
    query_data_N = N;
	data_N_original = N;

    iteration_num = iten;


	Input_Data = new double[(data_dim)*N];
	memcpy(Input_Data, Data, sizeof(double)*(data_dim)*N);

	Query_Data = new double[(data_dim)*query_data_N];
	memcpy(Query_Data, Data, sizeof(double)*(data_dim)*query_data_N);

	Query_Weight = new double[query_data_N];
	for (int i=0; i<query_data_N; i++)
	{
	  Query_Weight[i] = 1;
	}
	
    mode_total = new double[data_dim*data_N_original];
	mode_ind = new double[data_N_original];

	bandwid= band;
	
	//for query set covering
    sph_set = new sphere[data_N_original];
    sph_count_total = new double[iten];

	// Initialize the reference dataset and the associated weights
	ref_data_N = ref_data_n;
	Ref_Data = new double[(data_dim)*ref_data_N];
	memcpy(Ref_Data, ref_data, sizeof(double)*(data_dim)*ref_data_N);

	Ref_Weight = new double[ref_data_N];
	for (int i=0; i<ref_data_N; i++)
	{
	  Ref_Weight[i] = (double)(ref_weight[i]);
	}
}

Agglo_MS::~Agglo_MS()
{
	 if (Input_Data!=NULL)
	{
		delete[] Input_Data;
	}
	if (Query_Data!=NULL)
	{
		Query_Data = NULL;
	}
	if (Ref_Data!=NULL)
	{
		delete[] Ref_Data;
	}
	if (Ref_Weight!=NULL)
	{
		delete[] Ref_Weight;
	}
	if (mode_total!=NULL)
	{
		delete[] mode_total;
	}
	if (mode_ind!=NULL)
	{
		delete[] mode_ind;
	}
	if (sph_set!=NULL)
	{
		delete[] sph_set;
	}
	if (sph_count_total!=NULL)
	{
		delete[] sph_count_total;
	}

}



void Agglo_MS::sphere_construction(double* x_init, double weight)
{
    double* denum = new double[data_dim];
	memset(denum,0,sizeof(double)*data_dim);
	double num = 0;

	double* x_center = new double[data_dim];

	for (int j=0; j<data_dim; j++)
	{
		x_center[j] = x_init[j];
	}
	

	for ( int i=0; i<ref_data_N; i++)
	{
		double dist = 0;
		for (int j=0; j<data_dim; j++)
		{
			dist+= ((Ref_Data[i*data_dim+j]-x_init[j])/bandwid)*((Ref_Data[i*data_dim+j]-x_init[j])/bandwid);
		}
		double cur_w = exp(-dist);
		for ( int j=0; j<data_dim; j++)
		{
			denum[j]+=Ref_Weight[i]*cur_w*Ref_Data[i*data_dim+j];		
		}
		num+=Ref_Weight[i]*cur_w;
	}


	if (num>0)
	{
		for (int j=0; j<data_dim; j++)
		{
			x_center[j] = denum[j]/num;
		}	
		
	}
	else
	{
		for (int j=0; j<data_dim; j++)
		{
			x_center[j] = x_init[j];
		}	
	}

	//������
	double cur_radius = 0;
	for ( int j=0; j<data_dim; j++)
	{
		cur_radius+=(x_center[j]-x_init[j])*(x_center[j]-x_init[j]);
	}
	
	sph_set[sph_count].radius = cur_radius;
	sph_set[sph_count].center = new double[data_dim];
	memcpy(sph_set[sph_count].center, x_center, sizeof(double)*data_dim);
    sph_set[sph_count].weight = weight;

	sph_count++;
	
	delete[] denum;
	delete[] x_center;

}


void Agglo_MS::set_compression()
{
	double* x_init = new double[data_dim];
	
	double* mode_ind_cur = NULL;

	for (int l=0; l<iteration_num; l++)
	{
		sph_count = 0;

		mode_ind_cur = new double[query_data_N];



		for (int i=0; i<query_data_N; i++)
		{
			mode_ind_cur[i] = -1;

		}		
				
    	for ( int i=0; i<query_data_N; i++)
		{

		
		    bool m_ignore = false;

			double dist_cur = 0;

			for (int j=0; j<data_dim; j++)
			{
				x_init[j] = Query_Data[i*data_dim+j];	
			}


			for ( int k=0; k<sph_count; k++)
			{
				double dist_cur = 0;
				double mod_x = 0;
				for (int j=0; j<data_dim; j++)
				{
					dist_cur+=(x_init[j]-sph_set[k].center[j])*(x_init[j]-sph_set[k].center[j]);
					mod_x+= x_init[j]*x_init[j];
				}			
				double epslong = 1e-6;

				if ((dist_cur<=sph_set[k].radius)||(dist_cur<=epslong*mod_x))
				{

					sph_set[k].weight+=Query_Weight[i];
					m_ignore = true;
					mode_ind_cur[i] = (double)k;
					break;
				}
			}

			if (!m_ignore)
			{
				sphere_construction(x_init, Query_Weight[i]);
				mode_ind_cur[i] = (double)(sph_count-1);
			}
			
		}


		if (l==0)
		{
			for (int i=0; i<data_N_original; i++)
			{
				mode_ind[i] = mode_ind_cur[i];
			}
		}
		else
		{
			for (int i=0; i<data_N_original; i++)
			{
				mode_ind[i] = mode_ind_cur[(int)mode_ind[i]];
			}
		}

		delete[] Query_Data;
		delete[] Query_Weight;
		delete[] mode_ind_cur;


		query_data_N = sph_count;
		Query_Data = new double[data_dim*query_data_N];
		Query_Weight = new double[query_data_N];


		for (int j=0; j<query_data_N; j++)
		{
			for (int k=0; k<data_dim; k++)
			{
				Query_Data[j*data_dim+k] = sph_set[j].center[k];
			}
			Query_Weight[j] = sph_set[j].weight;

		}

	}

    delete[] x_init;

	sph_count_total[0] = double(sph_count);

}