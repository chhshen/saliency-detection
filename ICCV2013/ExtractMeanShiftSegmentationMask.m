clear all; close all; 
addpath(genpath('./feature_util'));
addpath(genpath('./edison_matlab_interface'));
run('./vlfeat/toolbox/vl_setup');

ImgDir = dir(fullfile('./Images/', '*.jpg'));
for tt=1:length(ImgDir)
    str = ImgDir(tt).name;
    ImageName = ['./Images/' str];
    %read the image
    org_img = imread([ImageName]);
    im = im2single(org_img);
    [fimage, labels, modes, regSize] = edison_wrapper(im, @ExtractFeature, 'SpatialBandWidth', 7, 'RangeBandWidth', 12, 'MinimumRegionArea', 200);
    segments = labels + 1;
    
    MatSaveDir = './MeanShiftSegDir/';
    if ~exist(MatSaveDir)
        mkdir(MatSaveDir); 
    end
    
    DestMatFilePath = sprintf('%s%s_ms.mat', MatSaveDir, str(1:end-4));
    save(DestMatFilePath, 'segments');
    tt
end