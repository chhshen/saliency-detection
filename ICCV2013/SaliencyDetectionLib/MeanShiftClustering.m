function [ModeIdxArr, ModeNumArr] = MeanShiftClustering(ColorFeatArr, ScaleArr)
%% mean shift clustering
% initialization
K = 120; [dim,datnum]=size(ColorFeatArr);
[rx,ClusterIndex,ClusterCenter,NumPoints,ClusterRadii]=KCenterClustering_mex(dim,datnum,ColorFeatArr,double(K));
Data_Source = ClusterCenter;
Weight_Source = double(NumPoints);
ite_num  = 500;

% clustering
ModeIdxArr = cell(length(ScaleArr), 1); ModeNumArr = zeros(length(ScaleArr), 1);
for kk=1:length(ScaleArr)
    band_compress = ScaleArr(kk);
    [sph, mode_idx, sph_count] = agglo_ms_mex(ColorFeatArr, Data_Source, double(Weight_Source), band_compress, ite_num);
    ModeIdxArr{kk} = mode_idx;
    ModeNumArr(kk) = sph_count;
end

end