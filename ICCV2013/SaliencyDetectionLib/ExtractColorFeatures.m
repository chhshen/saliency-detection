function [ColorArr, FeatArr] = ExtractColorFeatures(im, spstats)

image_lab = rgb2lab(im);
L = image_lab(:,:,1)./100;
a = image_lab(:,:,2)./128;
b = image_lab(:,:,3)./128;

image_hsv = rgb2hsv(im);
H = image_hsv(:,:,1);
ind = find(H > 0.5);
H(ind) = 1 - H(ind);

ColorFeaMat = cat(3, L, a, b, H);
ColorArr = reshape(ColorFeaMat, [size(im,1)*size(im,2) size(ColorFeaMat,3)]);


SPFea = [];
for ii=1:length(spstats)
    tmp_sgn = sign(ColorArr(spstats(ii).PixelIdxList, :));
    tmp_abs = abs(ColorArr(spstats(ii).PixelIdxList, :));
    
    Ac13 = mean(tmp_sgn.*tmp_abs.^(1/3), 1);
    Ac = mean(ColorArr(spstats(ii).PixelIdxList, :), 1);
    Ac3 = mean(ColorArr(spstats(ii).PixelIdxList, :).^3, 1);
    
    Mc13 = median(tmp_sgn.*tmp_abs.^(1/3), 1);
    Mc = median(ColorArr(spstats(ii).PixelIdxList, :), 1);
    Mc3 = median(ColorArr(spstats(ii).PixelIdxList, :).^3, 1);
    SPFea = [SPFea; [Ac13 Ac Ac3 Mc13 Mc Mc3 length(spstats(ii).PixelIdxList)]];
end
FeatArr = SPFea(:,1:end-1)';