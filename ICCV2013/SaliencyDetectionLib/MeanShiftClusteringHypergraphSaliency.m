function FinalContourMap = MeanShiftClusteringHypergraphSaliency(Img_grad, ColorFeatArr, spstats, num_region, m, n)

%% aggomelartive mean shift scales
ScaleArr = [0.15 0.25 0.35 0.45 0.55 0.65 0.75];
[ModeIdxArr, ModeNumArr] = MeanShiftClustering(ColorFeatArr, ScaleArr);

% multi-scale hypergraph analysis 
FinalContourMap = zeros(m,n); 
for dval_idx = 1:length(ScaleArr)
    if ModeNumArr(dval_idx)==1
        continue;
    end
    
    %% superpixel mode statistics
    mode_idx = ModeIdxArr{dval_idx};
    ClusteringMap = zeros(m,n);
    for ii=1:num_region
        ClusteringMap(spstats(ii).PixelIdxList) = mode_idx(ii);
    end
    cluster_spstats = regionprops(ClusteringMap+1, 'PixelIdxList');
    %figure('position',[60 120 1*n 1*m]); clf;
    %set(gcf,'DoubleBuffer','on','MenuBar','none');
    %imagesc(ClusteringMap); 
  
    %% Gradient information filtered by mode boundaries
    G=fspecial('gaussian',5,1.5);
    ClusteringMap_smooth=conv2(ClusteringMap+1,G,'same');  % smooth image by Gaussiin convolution
    [Mx,My]=gradient(ClusteringMap_smooth);
    Map_grad=Mx.^2+My.^2;
    Map_grad = double(Map_grad>0);   
    Map_grad = Map_grad.*Img_grad;
    %imagesc(Map_grad); colormap('gray');
   
    
    %% Image boundary penalty
    MinusInf = -1; % or MinusInf = -1000; tune this parameter to avoid some exceptional cases 
    Map_grad(1:16,:) = MinusInf; Map_grad(end-15:end,:) = MinusInf;
    Map_grad(:, 1:16) = MinusInf; Map_grad(:, end-15:end) = MinusInf;
    
    ImageBorderPrior = zeros(m,n);
    ImageBorderPrior(1:16,:) = MinusInf; ImageBorderPrior(end-15:end,:) = MinusInf;
    ImageBorderPrior(:, 1:16) = MinusInf; ImageBorderPrior(:, end-15:end) = MinusInf;
    

    ContourSalMap = zeros(m,n);
    for pp=1:ModeNumArr(dval_idx)
        ImgBorderVal = sum(ImageBorderPrior(cluster_spstats(pp).PixelIdxList));
        if ImgBorderVal<0
            sal_val = sum(Map_grad(cluster_spstats(pp).PixelIdxList)) + ImgBorderVal;
        else
            sal_val = sum(Map_grad(cluster_spstats(pp).PixelIdxList));
        end
        ContourSalMap(cluster_spstats(pp).PixelIdxList) = sal_val;
    end
    
    % remove the negative saliency pixels
    SalValArrU = unique(ContourSalMap(:));
    SalIdx = find(SalValArrU<0);
    if length(SalIdx)~=0
        ContourSalMap = ContourSalMap.*double(ContourSalMap>SalIdx(end));
    end
    ContourSalMap = (ContourSalMap - min(ContourSalMap(:))) / (max(ContourSalMap(:)) - min(ContourSalMap(:)) + eps);
    
    %% scale weighting
    FinalContourMap = FinalContourMap + 2^dval_idx*ContourSalMap;
    
end
% Linear normalization
FinalContourMap = (FinalContourMap - min(FinalContourMap(:))) / (max(FinalContourMap(:)) - min(FinalContourMap(:)) + eps);

%% postprocessing that is optional
%BW2 = imfill(FinalContourMap,'holes');
%FinalContourMap = FinalContourMap + double(BW2>0.5);
%FinalContourMap = histeq(FinalContourMap, 256);

figure, imagesc(FinalContourMap); colormap('gray'); title('Mean shift hypergraph saliency map');
end