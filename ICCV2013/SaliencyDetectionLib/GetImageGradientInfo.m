function Img_grad = GetImageGradientInfo(im)

sigma=1.5; % scale parameter in Gaussian kernel
G=fspecial('gaussian',5,sigma);
Img_smooth=conv2( double( rgb2gray( uint8( im )  ) ), G, 'same'); % smooth image by Gaussiin convolution
[Ix,Iy]=gradient(Img_smooth);
Img_grad=Ix.^2+Iy.^2; Org_Img_grad = Img_grad;
%figure, imagesc(Img_grad);  colormap('gray');


Img_Grad_T = prctile(Img_grad(:),90);
Arr = sort(Img_grad(:), 'descend');
idx = find(Arr>=Img_Grad_T);
%figure, plot(sqrt(Arr));
PixelNum = length(Img_grad(:));

% hold on; plot([1:PixelNum], ones(1, PixelNum)*sqrt(Img_Grad_T), 'r.');
% xlabel('Pixel index'); ylabel('Sorted gradient magnitude');
% hold off;

Img_grad = double(Img_grad>Img_Grad_T);
%figure, imagesc(Img_grad); colormap('gray');
