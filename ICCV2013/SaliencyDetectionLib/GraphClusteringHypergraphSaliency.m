function FinalContourMap2 = GraphClusteringHypergraphSaliency(im, Img_grad, m, n)

%% multi-scale graph clustering parameters
FinalContourMap2 = zeros(m,n);
seg_paras = [
    0.5 100 1200;
    1.0 100	1200;
    1.5 100	1200;
    2.0	100	1200;
    2.5	100	1200;
    3.0	100	1200;
    3.5	100	1200;
    4.0	100	1200;
    4.5	100	1200;
    5.0	100	1200;
    5.5	100	1200;
    ];

for rr=1:size(seg_paras, 1)
    %% graph clustering
    imsegs = im2superpixels(im, seg_paras(rr,:));
    if imsegs.nseg==1
        continue;
    end
    spstats_graph = regionprops(imsegs.segimage , 'PixelIdxList');
    
    % superpixels by graph clustering
    ClusteringMap = zeros(m,n);
    for ii=1:imsegs.nseg
        ClusteringMap(spstats_graph(ii).PixelIdxList) = ii;
    end
    cluster_spstats = regionprops(ClusteringMap, 'PixelIdxList');
    %figure('position',[60 120 1*n 1*m]); clf;
    %set(gcf,'DoubleBuffer','on','MenuBar','none');
    %imagesc(ClusteringMap); title('Ameanshift');
    
    %% gradient information
    G=fspecial('gaussian',5,1.5);
    ClusteringMap_smooth=conv2(ClusteringMap,G,'same');  % smooth image by Gaussiin convolution
    [Mx,My]=gradient(ClusteringMap_smooth);
    Map_grad=Mx.^2+My.^2;
    Map_grad = double(Map_grad>0);
    Map_grad = Map_grad.*Img_grad;
    
    
   %% Image boundary penalty
    MinusInf = -1000; % or MinusInf = -1; tune this parameter to avoid some exceptional cases 
    Map_grad(1:16,:) = MinusInf; Map_grad(end-15:end,:) = MinusInf;
    Map_grad(:, 1:16) = MinusInf; Map_grad(:, end-15:end) = MinusInf;
    
    ImageBorderPrior = zeros(m,n);
    ImageBorderPrior(1:16,:) = MinusInf; ImageBorderPrior(end-15:end,:) = MinusInf;
    ImageBorderPrior(:, 1:16) = MinusInf; ImageBorderPrior(:, end-15:end) = MinusInf;
    
    
    ContourSalMap = zeros(m,n);
    for pp=1:imsegs.nseg
        ImgBorderVal = sum(ImageBorderPrior(cluster_spstats(pp).PixelIdxList));
        if ImgBorderVal<0
            sal_val = sum(Map_grad(cluster_spstats(pp).PixelIdxList))+ImgBorderVal;
        else
            sal_val = sum(Map_grad(cluster_spstats(pp).PixelIdxList));
        end
        ContourSalMap(cluster_spstats(pp).PixelIdxList) = sal_val;
    end
    % remove the negative saliency pixels
    SalValArrU = unique(ContourSalMap(:));
    SalIdx = find(SalValArrU<0);
    if length(SalIdx)~=0
        ContourSalMap = ContourSalMap.*double(ContourSalMap>SalIdx(end));
    end
     ContourSalMap = (ContourSalMap - min(ContourSalMap(:))) / (max(ContourSalMap(:)) - min(ContourSalMap(:)) + eps); 
    
    %% saliency weighting
    FinalContourMap2 = FinalContourMap2 + 2^(seg_paras(rr,1))*ContourSalMap;   
end
FinalContourMap2 = (FinalContourMap2 - min(FinalContourMap2(:))) / (max(FinalContourMap2(:)) - min(FinalContourMap2(:)) + eps);

%% postprocessing that is optional
%BW2 = imfill(FinalContourMap2,'holes');
%FinalContourMap2 = FinalContourMap2 + double(BW2>0.5);
%FinalContourMap2 = histeq(FinalContourMap2, 256);
figure, imagesc(FinalContourMap2); colormap('gray'); title('Graph clustering hypergraph saliency map');

end
